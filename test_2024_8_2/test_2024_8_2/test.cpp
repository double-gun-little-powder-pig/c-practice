#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
int main()
{
	int year, month, day;
	int data[] = { 31,28,31,30,31,30,31,30,31,31,30,31,30,31 };
	while (cin >> year >> month >> day)
	{
		int sum = 0;
		for (int i = 0; i < month - 1; i++)
		{
			sum += data[i];
		}
		if (((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) && month > 2)
		{
			sum = sum + 1 + day;
		}
		else
		{
			sum = sum + day;
		}
		cout << sum << endl;
	}
	return 0;
}