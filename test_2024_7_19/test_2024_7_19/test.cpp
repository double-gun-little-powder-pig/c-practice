#define _CRT_SECURE_NO_WARNINGS
#include"Date.h"
int main()
{
	Date d1(2024, 7, 19);
	Date d2 = d1 + 5;
	//Date d3(d1 + 100);
	d1.Print();
	d2.Print();

	//d1 += 100;
	//d1.Print();

	d1 += 10000;
	d1.Print();

	return 0;
}