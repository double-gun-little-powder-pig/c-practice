#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
using namespace std;
//class stack
//{
//	void push()  //定义成员函数
//	{
//
//	}
//	int top()
//	{
//
//	}
//
//	int* a;      //定义成员变量
//	int top;
//	int capacity;
//};
//int main()
//{
//	return 0;
//}
class Date
{
public:
	//void Init(Date*const this,int year,int month,int day)
	void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//void Print(Date*const this)
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};
int main()
{
	Date d1;
	Date d2;
	//d1.Init(&d1,2024, 7, 17);
	d1.Init(2024, 7, 17);
	//d1.Print(&d1);
	d1.Print();

	//d2.Init(&d2,2024, 7, 18);
	d2.Init(2024, 7, 18);
	//d2.Print(&d2);
	d2.Print();
	return 0;
}